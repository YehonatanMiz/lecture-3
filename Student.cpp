#include "Student.h"

void Student::setId(const int id)
{
	if (id > 0) {
		_id = id;
	}
}

int Student::getId() const
{
	return _id;
}

unsigned int Student::getGradeByIndex(int index) const
{
	if (index >= 0 && index <= 3) {
		return _grades[index];
	}
}
void Student::setGradeByIndex(int index, unsigned int grade)
{
	_grades[index] = grade;
}

Student::Student(int id, string firstName, string lastName):
	_id(id), _firstName(firstName), _lastName(lastName)
{
	_grades = new unsigned int[NUM_OF_GRADES];
	int i;
	for (i = 0; i < NUM_OF_GRADES; i++)
	{
		_grades[i] = EMPTY_GRADE;
	}
}

Student::Student(const Student& other)
{
	std::cout << "Some one copy me!!" << std::endl;
	_id = other._id;
	_firstName = other._firstName;
	_lastName = other._lastName;
	_grades = new unsigned int[NUM_OF_GRADES];

}

Student::Student()
{
	std::cout << "c'tor" << std::endl;
	_id = 0;
	_firstName = "unknow";
	_lastName = "unknow";
	_grades = new unsigned int[NUM_OF_GRADES];
	int i;
	for (i = 0; i < NUM_OF_GRADES; i++)
	{
		_grades[i] = EMPTY_GRADE;
	}
}

Student::~Student()
{
	std::cout << "d'tor" << std::endl;
	delete[] _grades;
}