// grades array access
#define NUM_OF_GRADES 4
#define HISTORY_GRADE_IDX 0
#define MATH_GRADE_IDX 1
#define LITERATURE_GRADE_IDX 2
#define ENGLISH_GRADE_IDX 3
#define EMPTY_GRADE 101

#include <string>
#include <iostream>
using std::string;

class Student {

private:
	int _id;
	string _firstName;
	string _lastName;
	unsigned int* _grades;

	

public:
	Student(int id, string firstName, string lastName);
	Student(const Student& other);
	Student();
	~Student();
	void setId(const int id);
	int getId() const;

	unsigned int getGradeByIndex(int index) const;
	void setGradeByIndex(int index, unsigned int grade);


};