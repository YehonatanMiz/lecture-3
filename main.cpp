#include "Student.h"
bool compare(Student a, Student b);

int main() 
{
	Student a(1, "Lero", "Ofek");
	Student b(a);
	bool c = compare(a, b);

	std::cout << c << std::endl;


	return 0;
}

bool compare(Student a, Student b) {

	return a.getId() == b.getId();
}